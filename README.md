# objective cage


## One-Sentence-Pitch

Protects your calibration objective. 


## How does it work?
Zeiss laser scanning microscopes depend on a specialized, highly expensive objective for calibration. This objective features a cap that contains a fluorescent slide with a metal pattern. To ensure proper centering and rotation, the cap is adjusted and secured with three delicate and tiny grub screws. As a result, the lens should not be touched or lifted by the cap. However, when dealing with a system equipped with an aqua-stop, this becomes unavoidable, as the calibration objective cannot be screwed in or out without tightening the grub screws. This action gradually wears out the screws and damages the v-groove, rendering the objective unusable. To address this issue, I designed a protective cage for the objective that enables installation without touching the rotating cap, while still allowing access to and manipulation of the screws if adjustments are necessary.

With the protective cage in place, the objective can be handled safely, which is crucial since a facility typically has only one such objective. As a result, it is frequently moved from one microscope to another.

<img src="mounted.jpg" width=30% />

This is also not a problem with an aqua-stop:

<img src="mountedwithAquastop.jpg" width=30% />

To print your own use a flexible material such as PETG as the rings are fitting tightly around the shell parts.
Print in this configuration and with 100% infill: 

<img src="printlikethis.jpg" width=30% />

The assembly should be straightforward. If you feel that, too much force is required to get the rings on, lightly sand any imperfections of the print away: 

![Assembly instructions](HowToAssemble.mp4)

Once assembled the objective cap can be centered very accurately and the alignment screws can ever so slightly be tightened as to allow for a smooth rotation. After this initial adjustment, if you keep handling the objective by the cage, no further realignment should ever be necessary. Always carry the objective by the shell and don't lift it by the top ring only.

## Status

It works nicely and has already been shared with other facilities. If you have a LSM calibration objective then please use it. Please feel free to contact me and I will send you one.

## Acknowledgment

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.

